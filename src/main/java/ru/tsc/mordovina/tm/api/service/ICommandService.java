package ru.tsc.mordovina.tm.api.service;

import ru.tsc.mordovina.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

